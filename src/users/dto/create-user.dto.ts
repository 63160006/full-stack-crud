import { IsNotEmpty, Length } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  // @Length(3, 16)
  username: string;

  @IsNotEmpty()
  // @Length(3, 16)
  name: string;

  @IsNotEmpty()
  // @Length(5, 16)
  password: string;
}
